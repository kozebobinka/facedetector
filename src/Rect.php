<?php

namespace Kozebobina\FaceDetector;

class Rect
{
    public int $x1;
    public int $x2;
    public int $y1;
    public int $y2;
    public float $weight;

    public function __construct(int $x1, int $x2, int $y1, int $y2, float $weight)
    {
        $this->x1 = $x1;
        $this->x2 = $x2;
        $this->y1 = $y1;
        $this->y2 = $y2;
        $this->weight = $weight;
    }

    public static function fromString($text): self
    {
        $tab = explode(" ", $text);
        $x1 = intval($tab[0]);
        $x2 = intval($tab[1]);
        $y1 = intval($tab[2]);
        $y2 = intval($tab[3]);
        $f = floatval($tab[4]);

        return new Rect($x1, $x2, $y1, $y2, $f);
    }
}