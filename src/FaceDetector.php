<?php declare(strict_types=1);

namespace Kozebobina\FaceDetector;

class FaceDetector
{
    private const FRONTAL_FACE_DEFAULT =  __DIR__ . "/../data/haarcascade_frontalface_default.xml";
    private const FRONTAL_FACE_ALT =  __DIR__ . "/../data/haarcascade_frontalface_alt.xml";

    private array $classifierSize = [];
    /**
     * @var Stage[]
     */
    private array $stages = [];
    private ?\GdImage $image = null;
    private ?int $width = null;
    private ?int $height = null;
    private array $foundRects = [];

    public function __construct(bool $useAltHaarCascade = true)
    {
        $this->initClassifier($useAltHaarCascade ? self::FRONTAL_FACE_ALT : self::FRONTAL_FACE_DEFAULT);
    }

    private function initClassifier(string $classifierFile): void
    {
        $xmls = \file_get_contents($classifierFile);
        $xmls = \preg_replace("/<!--[\S|\s]*?-->/", "", $xmls);
        $xml = \simplexml_load_string($xmls);

        $this->classifierSize = \explode(" ", \strval($xml->children()->children()->size));
        $this->stages = [];

        $stagesNode = $xml->children()->children()->stages;

        foreach ($stagesNode->children() as $stageNode) {
            $stage = new Stage(\floatval($stageNode->stage_threshold));

            foreach ($stageNode->trees->children() as $treeNode) {
                $feature = new Feature(\floatval($treeNode->_->threshold), \floatval($treeNode->_->left_val), \floatval($treeNode->_->right_val), $this->classifierSize);

                foreach ($treeNode->_->feature->rects->_ as $r) {
                    $feature->add(Rect::fromString(\strval($r)));
                }

                $stage->addFeature($feature);
            }

            $this->stages[] = $stage;
        }
    }

    /**
     * Detect faces in given image
     *
     * @param string $imageFile path of image file
     * @throws \Exception
     */
    public function scan(string $imageFile): void
    {
        $imageInfo = \getimagesize($imageFile);

        if (!$imageInfo) {
            throw new \Exception("Could not open file: " . $imageFile);
        }

        [$this->width, $this->height, $imageType] = $imageInfo;

        $this->image = match ($imageType) {
            \IMAGETYPE_JPEG => \imagecreatefromjpeg($imageFile),
            \IMAGETYPE_GIF => \imagecreatefromgif($imageFile),
            \IMAGETYPE_PNG => \imagecreatefrompng($imageFile),
            default => throw new \Exception("Unknown file format: " . $imageType . ", " . $imageFile),
        };

        $this->foundRects = [];

        $maxScale = \min($this->width / $this->classifierSize[0], $this->height / $this->classifierSize[1]);
        $grayImage = \array_fill(0, $this->width, \array_fill(0, $this->height, null));
        $squares = \array_fill(0, $this->width, \array_fill(0, $this->height, null));

        for ($i = 0; $i < $this->width; $i++) {
            $col = 0;
            $col2 = 0;
            for ($j = 0; $j < $this->height; $j++) {
                $colors = \imagecolorsforindex($this->image, \imagecolorat($this->image, $i, $j));

                $value = (30 * $colors['red'] + 59 * $colors['green'] + 11 * $colors['blue']) / 100;
                $grayImage[$i][$j] = ($i > 0 ? $grayImage[$i - 1][$j] : 0) + $col + $value;
                $squares[$i][$j] = ($i > 0 ? $squares[$i - 1][$j] : 0) + $col2 + $value * $value;
                $col += $value;
                $col2 += $value * $value;
            }
        }

        $baseScale = 2;
        $scale_inc = 1.25;
        $increment = 0.1;

        for ($scale = $baseScale; $scale < $maxScale; $scale *= $scale_inc) {
            $step = (int)($scale * 24 * $increment);
            $size = (int)($scale * 24);

            for ($i = 0; $i < $this->width - $size; $i += $step) {
                for ($j = 0; $j < $this->height - $size; $j += $step) {
                    $pass = true;
                    $k = 0;
                    foreach ($this->stages as $s) {

                        if (!$s->pass($grayImage, $squares, $i, $j, $scale)) {
                            $pass = false;
                            break;
                        }
                        $k++;
                    }
                    if ($pass) {
                        $this->foundRects[] = array("x" => $i, "y" => $j, "width" => $size, "height" => $size);
                    }
                }
            }
        }
    }

    /**
     * Returns array of found faces.
     *
     * Each face is represented by an associative array with the keys x, y, width and height.
     *
     * @param bool $moreConfidence desire more confidence what a face is, gives fewer results
     * @return mixed[] found faces
     */
    public function getFaces(bool $moreConfidence = false): array
    {
        return $this->merge($this->foundRects, 2 + intval($moreConfidence));
    }

    /**
     * Gives access to image with found faces marked
     *
     * @param string|null $fileName filename to save on disk
     * @param bool $moreConfidence desire more confidence what a face is, gives fewer results
     * @param bool $showAllRects mark all faces before merging, for debugging purposes
     * @return bool|\GdImage if filename given, image will be saved to disk, otherwise image resource
     * @throws \Exception
     */
    public function getImage(string $fileName = null, bool $moreConfidence = false, bool $showAllRects = false): bool|\GdImage
    {
        $canvas = \imagecreatetruecolor($this->width, $this->height);
        \imagecopyresampled($canvas, $this->image, 0, 0, 0, 0, $this->width, $this->height, $this->width, $this->height);

        $blue = \imagecolorallocate($canvas, 0, 0, 255);
        $red = \imagecolorallocate($canvas, 255, 0, 0);

        if ($showAllRects) {
            foreach ($this->foundRects as $r) {
                \imagerectangle($canvas, $r['x'], $r['y'], $r['x'] + $r['width'], $r['y'] + $r['height'], $blue);
            }
        }

        $rects = $this->merge($this->foundRects, 2 + intval($moreConfidence));
        foreach ($rects as $r) {
            \imagerectangle($canvas, $r['x'], $r['y'], $r['x'] + $r['width'], $r['y'] + $r['height'], $red);
        }

        if (empty($fileName)) {
            return $canvas;
        }

        $fileNameParts = \explode('.', $fileName);
        $ext = \strtolower(\array_pop($fileNameParts));

        return match ($ext) {
            "jpg" => \imagejpeg($canvas, $fileName, 100),
            "gif" => \imagegif($canvas, $fileName),
            "png" => \imagepng($canvas, $fileName),
            default => throw new \Exception("Unknown file format: " . $ext),
        };
    }

    private function merge($rects, $min_neighbors): array
    {
        $retour = [];
        $ret = [];
        $nb_classes = 0;

        $countRects = \count($rects);
        for ($i = 0; $i < $countRects; $i++) {
            $found = false;
            for ($j = 0; $j < $i; $j++) {
                if ($this->equals($rects[$j], $rects[$i])) {
                    $found = true;
                    $ret[$i] = $ret[$j];
                }
            }

            if (!$found) {
                $ret[$i] = $nb_classes;
                $nb_classes++;
            }
        }

        $neighbors = [];
        $rect = [];
        for ($i = 0; $i < $nb_classes; $i++) {
            $neighbors[$i] = 0;
            $rect[$i] = array("x" => 0, "y" => 0, "width" => 0, "height" => 0);
        }

        foreach ($rects as $i => $rectsItem) {
            $neighbors[$ret[$i]]++;
            $rect[$ret[$i]]['x'] += $rectsItem['x'];
            $rect[$ret[$i]]['y'] += $rectsItem['y'];
            $rect[$ret[$i]]['width'] += $rectsItem['width'];
            $rect[$ret[$i]]['height'] += $rectsItem['height'];
        }

        for ($i = 0; $i < $nb_classes; $i++) {
            $n = $neighbors[$i];
            if ($n >= $min_neighbors) {
                $r['x'] = ($rect[$i]['x'] * 2 + $n) / (2 * $n);
                $r['y'] = ($rect[$i]['y'] * 2 + $n) / (2 * $n);
                $r['width'] = ($rect[$i]['width'] * 2 + $n) / (2 * $n);
                $r['height'] = ($rect[$i]['height'] * 2 + $n) / (2 * $n);

                $retour[] = $r;
            }
        }
        return $retour;
    }

    private function equals(array $r1, array $r2): bool
    {
        $distance = (int)($r1['width'] * 0.2);

        if ($r2['x'] <= $r1['x'] + $distance &&
            $r2['x'] >= $r1['x'] - $distance &&
            $r2['y'] <= $r1['y'] + $distance &&
            $r2['y'] >= $r1['y'] - $distance &&
            $r2['width'] <= (int)($r1['width'] * 1.2) &&
            (int)($r2['width'] * 1.2) >= $r1['width']) {
            return true;
        }

        if ($r1['x'] >= $r2['x'] &&
            $r1['x'] + $r1['width'] <= $r2['x'] + $r2['width'] &&
            $r1['y'] >= $r2['y'] &&
            $r1['y'] + $r1['height'] <= $r2['y'] + $r2['height']) {
            return true;
        }

        return false;
    }
}
