<?php

namespace Kozebobina\FaceDetector;

class Feature
{
    public array $rects;
    public float $threshold;
    public float $left_val;
    public float $right_val;
    public array $size;

    public function __construct($threshold, $left_val, $right_val, $size)
    {

        $this->rects = [];
        $this->threshold = $threshold;
        $this->left_val = $left_val;
        $this->right_val = $right_val;
        $this->size = $size;
    }

    public function add(Rect $r): void
    {
        $this->rects[] = $r;
    }

    public function getVal($grayImage, $squares, $i, $j, $scale): float
    {
        $w = (int)($scale * $this->size[0]);
        $h = (int)($scale * $this->size[1]);
        $inv_area = 1 / ($w * $h);

        $total_x = $grayImage[$i + $w][$j + $h] + $grayImage[$i][$j] - $grayImage[$i][$j + $h] - $grayImage[$i + $w][$j];
        $total_x2 = $squares[$i + $w][$j + $h] + $squares[$i][$j] - $squares[$i][$j + $h] - $squares[$i + $w][$j];

        $moy = $total_x * $inv_area;
        $vnorm = $total_x2 * $inv_area - $moy * $moy;
        $vnorm = ($vnorm > 1) ? sqrt($vnorm) : 1;

        $rect_sum = 0;
        foreach ($this->rects as $kValue) {
            $r = $kValue;
            $rx1 = $i + (int)($scale * $r->x1);
            $rx2 = $i + (int)($scale * ($r->x1 + $r->y1));
            $ry1 = $j + (int)($scale * $r->x2);
            $ry2 = $j + (int)($scale * ($r->x2 + $r->y2));

            $rect_sum += (int)(($grayImage[$rx2][$ry2] - $grayImage[$rx1][$ry2] - $grayImage[$rx2][$ry1] + $grayImage[$rx1][$ry1]) * $r->weight);
        }

        $rect_sum2 = $rect_sum * $inv_area;

        return ($rect_sum2 < $this->threshold * $vnorm ? $this->left_val : $this->right_val);
    }
}