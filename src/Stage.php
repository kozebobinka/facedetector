<?php

namespace Kozebobina\FaceDetector;

class Stage
{
    /**
     * @var Feature[]
     */
    private array $features;
    private float $threshold;

    public function __construct(mixed $threshold)
    {
        $this->threshold = floatval($threshold);
        $this->features = array();
    }

    public function pass($grayImage, $squares, $i, $j, $scale): bool
    {
        $sum = 0;
        foreach ($this->features as $feature) {
            $sum += $feature->getVal($grayImage, $squares, $i, $j, $scale);
        }

        return $sum > $this->threshold;
    }

    public function addFeature(Feature $feature): void
    {
        $this->features[] = $feature;
    }
}