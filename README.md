# FaceDetector

Made from this repository: https://github.com/felixkoch/PHP-FaceDetector

This works with default or alternative Haas cascades, depends on constructor argument 

Basic usage:

```php
<?php
use Rentberry\FaceDetector\FaceDetector

$detector = new FaceDetector();
$detector->scan("test.jpg");
$faces = $detector->getFaces();
foreach($faces as $face)
{
  echo "Face found at x: {$face['x']}, y: {$face['y']}, width: {$face['width']}, height: {$face['height']}<br />\n"; 
}
```